<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $name = 'films';
    protected $fillable =['name','director_id','genre','description','year','award_id','price','cover_img','screenshots'];
    public function director()
    {
        return $this->belongsTo(\App\Director::class);
    }
    public function awards()
    {
        return $this->belongsToMany(\App\Award::class);
    }
}
