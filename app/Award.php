<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    protected $table = 'awards';
    protected $fillable = 'name';
    public function films()
    {
        return $this->hasMany(\App\Film::class);
    }
}
