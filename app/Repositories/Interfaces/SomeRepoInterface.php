<?php
/**
 * Created by PhpStorm.
 * User: daddits
 * Date: 23.10.18
 * Time: 18:33
 */

namespace App\Repositories\Interfaces;

use App\Director;

interface SomeRepoInterface
{
    public function createDirector(array $data):Director
}