<?php
/**
 * Created by PhpStorm.
 * User: daddits
 * Date: 22.10.18
 * Time: 22:58
 */

namespace App\Repositories\Interfaces;

use App\Director;

interface DirectorsInterface
{
    public function createDirector(array $data):Director
}