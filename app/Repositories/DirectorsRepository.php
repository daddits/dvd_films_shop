<?php
/**
 * Created by PhpStorm.
 * User: daddits
 * Date: 22.10.18
 * Time: 23:18
 */

namespace App\Repositories;
use App\Director;
use App\Repositories\Interfaces\DirectorsInterface;
use Illuminate\Http\Request;

class DirectorsRepository
{
    //При использовании интерфейса - ошибка class DirectorsRepository noy found
    /**
     * @var Director     */
    private $director;

    public function __construct(Director $director)
    {
        $this->director = $director;
    }

    public function displayDirectorsList()
    {
        return $directors = $this->director->all();
    }
    /**
     * creating new director
     * @param array $data
     * @return Director
     */
    public function createDirector(array $data): Director
    {
        return $this->director->create($data);
    }

    /**
     * @param $data
     */
    public function updateDirector($data)
    {
        $director = $this->director->find($data['id']);
        $director->name = $data['name'];//Linklaiter
        $director->save();
    }

    /**
     * @return Director[]|\Illuminate\Database\Eloquent\Collection
     */
    public function allDirectors()
    {
        return $this->director->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findDirector($id)
    {
        return $this->director->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findDirectorById($id)
    {
        return $this->director->where('id',$id);
    }

    /**
     * @return bool|null
     * @throws \Exception
     */
    public function deleteDirector()
    {
        return $this->director->delete();
    }
}