<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
   protected $name = 'countries';
   protected $fillable = 'name';
}
