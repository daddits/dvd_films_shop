<?php

namespace App\Http\Controllers;
use App\Exceptions\UserNotFoundException;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    public function index()
    {
        return view('users.index');
    }

    public function search(Request $request)
    {
        try {
            $user = User::findOrFail($request->input('user_id'));
        } catch (\Exception $exception) {
            throw new UserNotFoundException($exception->getMessage());
        }
        return view('users.search', compact('user'));
    }
}
