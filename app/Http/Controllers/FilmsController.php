<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Film;
use App\Director;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
class FilmsController extends Controller
{
    private $film;
    private $director;

    public function __construct(Film $film, Director $director)
    {
        $this->film = $film;
        $this->director = $director;

    }

    /**
     * describing list of films
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
       $films = $this->film->get();
        return view('films.index',compact('films',$films));
    }

    /**
     * form for creating film
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $directors = $this->director->get();
        //dd($directors);
        return view('films.create',compact('directors', $directors));
    }
    /**
     * store new film from form
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //dd($request);
        $newfilm = [
            'name'=>$request->name,
            'director_id'=>$request->director_id,
            'genre'=>$request->genre,
            'description'=>$request->description,
            'year'=>$request->year,
            'award_id'=>$request->award,
            'price'=>$request->price,
            'cover_img'=>"image-default.png",
            'screenshots'=>"image-default.png"
        ];
        $this->film->create($newfilm);
        Session::flash('message', 'Успешно добавлен новый фильм');
        return redirect()->back();
    }

    /**
     * describing one of films
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $film = $this->film->where('id',$id)->get();
        return view('films.show',compact('film',$film));
    }

    /**
     * deleting film
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $deletedFilm = $this->film->where('id', $id)->delete();
        Session::flash('message', 'Успешно удален');
        return redirect()->back();
    }

    /**
     * editing film information
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $film =$this->film->find($id);
        $directors = $this->director->get();
        return view('films.update',compact('film','directors'));
    }
    /**
     * @param $id
     * @param Request $request
     * updating existing director
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request){
        //dd($request);
            $film = $this->film->find($id);
            $film->name = $request->name;
            $film->director_id = $request->director_id;
            $film->genre = $request->genre;
            $film->description = $request->description;
            $film->year = $request->year;
            $film->award = $request->award;
            $film->price = $request->price;
            $film->save();
            Session::flash('message', 'Успешно обновлена информация');
            return redirect()->action('FilmsController@index');
        }
}

