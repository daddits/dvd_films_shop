<?php

namespace App\Http\Controllers;


use App\Repositories\DirectorsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
class DirectorsController extends Controller
{
    private $directorRepository;

    public function __construct(DirectorsRepository $directorRepository)
    {
        $this->directorRepository = $directorRepository;
    }
    /**
     * Displaing list of directors
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $directors = $this->directorRepository->displayDirectorsList();
    return view('directors.index',compact('directors',$directors));
    }
    /**
     * Form for new Director
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('directors.create');
    }
    /**
     * Adding new director to Database
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $newDirector = [
            'name'=>$request->name,
        ];
        $this->directorRepository->createDirector($newDirector);
        Session::flash('message', 'Успешно добавлен новый режиссер');
        return redirect()->route('directors.index');
    }

    /**
     * Updating existing director
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request){
        $updatedDirector = [
            'name'=>$request->name,
            'id' =>$request->id
        ];
        $this->directorRepository->updateDirector($updatedDirector);
            Session::flash('message', 'Успешно обновлена информация');
            return redirect()->route('directors.index');
    }
    /**
     * @param $id
     * editing director
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $director =$this->directorRepository->findDirector($id);
        return view('directors.update',compact('director',$director));
    }
    /**
     * Deleting existing director
     * @param $id
     * deleting directir
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $deletedDirector = $this->directorRepository->findDirectorById($id);
        $deletedDirector->delete();
        Session::flash('message', 'Успешно удален режиссер');
        return redirect()->route('directors.index');
    }
}
