@extends('layouts.directors')
@section('content')
    <hr>
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table">
        <thead class="table-info">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Режиссер</th>
            <th scope="col">Страна</th>
            <th scope="col">Количество фильмов</th>
            <th scope="col">Редактировать</th>
            <th scope="col">Удалить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($directors as $director)
            <tr>
                <th>{{$director->id}}</th>
                <th>{{$director->name}}</th>
                <th>{{$director->country}}</th>
                <th>{{$director->film_count}}</th>
                <th><a href="/directors/edit/{{$director->id}}">Редактировать</a></th>
                <th><a href="/directors/delete/{{$director->id}}">Удалить</a></th>
            </tr>
        </tbody>
        @endforeach
    </table>
@endsection

