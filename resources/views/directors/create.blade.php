@extends('layouts.directors')
@section('content')
        {!! Form::open(['url'=>'directors/store','method'=>'post']) !!}
        <div class="form-group">
            {!! Form::label('name') !!}
            {!! Form::text('name', null, ['class'=>'form-control'] ) !!}
            <div>
                <div class="form-group">
                    {!! Form::submit('Добавить режиссера', ['class'=>'btn btn-primary']) !!}
                    <div>
        {!! Form::close()!!}
@endsection