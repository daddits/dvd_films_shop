@extends('layouts.directors')
@section('content')

        {!! Form::open(['url'=>"directors/update/{$director['id']}",'method'=>'post']) !!}
        <div class="form-group">
            {!! Form::label('name') !!}
            {!! Form::text('name',$director['name'], ['class'=>'form-control'] ) !!}
            <div>
                                        <div class="form-group">
                                            {!! Form::submit('Обновить режиссера', ['class'=>'btn btn-primary']) !!}
                                            <div>
                                                {!! Form::close()!!}
@endsection