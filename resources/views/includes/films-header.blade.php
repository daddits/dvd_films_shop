<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
    <h1>Список Фильмов</h1>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">На главную</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/films">Список фильмов</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/films/create">Добавить Фильм</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>

