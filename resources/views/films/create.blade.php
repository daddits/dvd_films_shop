@extends('layouts.films')
@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
       {!! Form::open(['url'=>'films/store','method'=>'post']) !!}
        <div class="form-group">
            <h3>{!! Form::label('Название фильма') !!}</h3>
            {!! Form::text('name', 'film1', ['class'=>'form-control'] ) !!}
        </div>
       <h3>{!! Form::label('Выбор режиссера') !!}</h3>
       {!! Form::select('directory_id', [
       '1' => 'Хе́йвуд «Ву́ди» А́ллен',
       '2' => 'Микела́нджело Антонио́ни',
       '3' =>'Андрей Тарковский',
       '4'=>'Ингмар Бергман',
       '5'=>'Франсуа Трюффо',
       '6'=>'Ра́йнер Ве́рнер Фа́сбиндер',
       '7'=>'Другой'],['class'=>'form-control']) !!}
       <div class="form-group">
           <h3>{!! Form::label('Описание фильма') !!}</h3>
           {!! Form::text('about', 'film description 1', ['class'=>'form-control'] ) !!}
       </div>
       <div class="form-group">
       <h3>{!! Form::label('Жанр фильма:') !!}</h3>
       <span>Драма{!! Form::checkbox('genre', 'drama', true) !!}</span>
       <span>Комедия{!! Form::checkbox('genre', 'comedy') !!}</span>
       <span>Триллер{!! Form::checkbox('genre', 'thriller') !!}</span>
       <span>Боевик{!! Form::checkbox('genre', 'fight') !!}</span>
       <span>Исторический{!! Form::checkbox('genre', 'history') !!}</span>
       <span>Мистика{!! Form::checkbox('genre', 'mistic') !!}</span>
       <span>Ужасы{!! Form::checkbox('genre', 'horor') !!}</span>
       <span>Мелодрама{!! Form::checkbox('genre', 'melo') !!}</span>
       <span>Спорт{!! Form::checkbox('genre', 'sport') !!}</span>
       <span>Прочее{!! Form::checkbox('genre', 'other') !!}</span>
       </div>
       <h3><div class="form-group"></h3>
           {!! Form::label('Сюжет фильма') !!}
           {!! Form::text('story', 'film1 story about1', ['class'=>'form-control'] ) !!}
       </div>
       <h3><div class="form-group"></h3>
           <h3>{!! Form::label('Аудитория') !!}</h3>
           <p>Авторское кино{!! Form::radio('isarthaus', '1', true) !!}</p>
           <p>Кассовое кино/ блокбастер{!! Form::radio('isarthaus', '0', true) !!}</p>
       </div>
       <div class="form-group">
           <h3>{!! Form::label('Известные актеры') !!}</h3>
           {!! Form::text('actors', 'Дэниэль Дей Льюис', ['class'=>'form-control'] ) !!}
       </div>
       <div class="form-group">
           <h3>{!! Form::label('Год') !!}</h3>
           {!! Form::number('year', 1990, ['class'=>'form-control'] ) !!}
       </div>
       <div class="form-group">
           <h3>{!! Form::label('Награды') !!}</h3>
           <p> {!! Form::checkbox('award', 'oskar', true) !!} Оскар</p>
           <p> {!! Form::checkbox('award', 'kann') !!} Каннский кинофестиваль</p>
           <p> {!! Form::checkbox('award', 'batfa') !!} Приз британской академии кино (BAFTA)</p>
           <p> {!! Form::checkbox('award', 'sezar') !!}Национальная кинопремия Франции (Сезар)</p>
           <p> {!! Form::checkbox('award', 'globus') !!}Золотой глобус</p>
           <p> {!! Form::checkbox('award', 'berlin') !!}Берлинский кинофестиваль</p>
       </div>
       <div class="form-group">
           <h3>{!! Form::label('Ссылка на imdb') !!}</h3>
           {!! Form::number('imdb',123456, ['class'=>'form-control'] ) !!}
       </div>
       <div class="form-group">
           <h3>{!! Form::label('Цена') !!}</h3>
           {!! Form::number('price',3.01, ['class'=>'form-control'] ) !!}
       </div>
        <div class="form-group">
            {!! Form::submit('Добавить фильм', ['class'=>'btn btn-primary']) !!}
            <div>

@endsection