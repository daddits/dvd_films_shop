@extends('layouts.films')
@section('content')
    <hr>
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table">
        <thead class="table-info">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название фильма</th>
            <th scope="col">Редактировать</th>
            <th scope="col">Удалить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($films as $film)
            <tr>
                <th>{{$film->id}}</th>
                <th><a href="/films/show/{{$film->id}}">{{$film->name}}</a></th>
                <th><a href="/films/edit/{{$film->id}}">Редактировать</a></th>
                <th><a href="/films/delete/{{$film->id}}">Удалить</a></th>
            </tr>
        </tbody>
        @endforeach
    </table>
@endsection

