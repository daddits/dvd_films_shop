@extends('layouts.films')
@section('content')
    <div class="container">

        <img src="/images/directors/default.png" width="300px" height="200px">

        <table class="table">
            <tbody>
            @foreach($film as $property)
            <tr>
                <td>Название фильма</td>
                <td><h3>{{$property->name}}</h3></td>
            </tr>
            <tr>
                <td>Режиссер</td>
                <td>{{$property->director_id}}</td>
            </tr>
            <tr>
                <td>Информация о фильме</td>
                <td>{{$property->about}}</td>
            </tr>
            <tr>
                <td>Жанр фильма</td>
                <td>{{$property->genre}}</td>
            </tr>
            <tr>
                <td>Сюжет фильма</td>
                <td>{{$property->description}}</td>
            </tr>
            <tr>
                <td>Кинопрокат</td>
                <td>{{$property->isarthaus}}</td>
            </tr>
            <tr>
                <td>Известные актеры</td>
                <td>{{$property->actors_list}}</td>
            </tr>
            <tr>
                <td>Год выпуска</td>
                <td>{{$property->year}}</td>
            </tr>
            <tr>
                <td>Награды</td>
                <td>{{$property->award}}</td>
            </tr>
            <tr>
                <td>Ссылка на IMDB</td>
                <td><a href="{{$property->imdb}}">{{$property->imdb}}</a></td>
            </tr>
            <tr>
                <td>Цена</td>
                <td>{{$property->price}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
        <a href="/addtocart" class="btn btn-primary">Добавить в корзину</a>
        <a href="/films/edit/{{$property->id}}" class="btn btn-success">Редактировать</a>
    </div>
@endsection