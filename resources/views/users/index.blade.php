@extends('layouts.app')
@section('content')
    {!! Form::open(['url'=>'users/search','method'=>'post']) !!}
    <div class="form-group">
        {!! Form::label('name') !!}
        {!! Form::number('user_id', null, ['class'=>'form-control'] ) !!}
        <div>
            <div class="form-group">
                {!! Form::submit('Найти Пользователя', ['class'=>'btn btn-primary']) !!}
                <div>
                    {!! Form::close()!!}

    @endsection