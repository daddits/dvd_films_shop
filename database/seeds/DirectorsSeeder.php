<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DirectorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $woodyallen = ['name'=>'Хе́йвуд «Ву́ди» А́ллен','country'=>'США', 'is_arthaus'=>true,'film_count'=>'2'];
        $antonioni = ['name'=>'Микела́нджело Антонио́ни','country'=>'Италия', 'is_arthaus'=>true,'film_count'=>'2'];
        $tarkovsky = ['name'=>'Андрей Тарковский','country'=>'СССР', 'is_arthaus'=>true,'film_count'=>'2'];
        $bergman = ['name'=>'Ингмар Бергман','country'=>'Швеция', 'is_arthaus'=>true,'film_count'=>'2'];
        $truffaut = ['name'=>'Франсуа Трюффо','country'=>'Франция', 'is_arthaus'=>true,'film_count'=>'2'];
        $fassbinder = ['name'=>'Ра́йнер Ве́рнер Фа́сбиндер','country'=>'Германия', 'is_arthaus'=>true,'film_count'=>'2'];
        DB::table('directors')->insert([$woodyallen, $antonioni, $tarkovsky,$bergman, $truffaut, $fassbinder] );
    }
}
