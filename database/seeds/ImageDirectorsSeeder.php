<?php

use Illuminate\Database\Seeder;

class ImageDirectorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $woodyallenimage = ['image_path'=>'/images/directors/woodyallen','directors_id'=>1];
        $antonioniimage  = ['image_path'=>'/images/directors/antonioni','directors_id'=>2];
        $tarkovskyimage  = ['image_path'=>'/images/directors/tarkovskiy','directors_id'=>3];
        $bergmanimage  = ['image_path'=>'/images/directors/bergman','directors_id'=>4];
        $truffautimage  = ['image_path'=>'/images/directors/truffo','directors_id'=>5];
        $fassbinderimage  = ['image_path'=>'/images/directors/fassbender','directors_id'=>6];
        DB::table('directors_images')->insert([
            $woodyallenimage ,
            $antonioniimage ,
            $tarkovskyimage ,
            $bergmanimage ,
            $truffautimage ,
            $fassbinderimage ] );
    }
}
