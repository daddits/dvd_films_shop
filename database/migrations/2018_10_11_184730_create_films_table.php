<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('director_id');
            $table->foreign('director_id')->references('id')->on('directors');
            $table->text('about');
            $table->string('genre');
            $table->text('description');
            $table->boolean('is_arthaus');
            $table->text('actors_list');
            $table->integer('year');
            $table->string('award')->default('no awards');
            $table->string('image_path')->default('cover.png');
            $table->string('imdb');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
