<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Exceptions\UserNotFoundException;
Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index');
Route::group(['prefix'=>'directors','middleware'=>'shopMiddleware'], function (){
    Route::get('/', 'DirectorsController@index')->name('directors.index');
    Route::get('/create', 'DirectorsController@create');
    Route::post('/store', 'DirectorsController@store');
    Route::post('/update/{id}', 'DirectorsController@update');
    Route::get('/edit/{id}', 'DirectorsController@edit');
    Route::get('/delete/{id}', 'DirectorsController@delete');
});
Route::get('/films', 'FilmsController@index')->name('films.index');
Route::group(['prefix'=>'films','middleware'=>'shopMiddleware'], function (){
    Route::get('/create', 'FilmsController@create');
    Route::post('/store', 'FilmsController@store');
    Route::post('/update/{id}', 'FilmsController@update');
    Route::get('/edit/{id}', 'FilmsController@edit');
    Route::get('/show/{id}', 'FilmsController@show');
    Route::get('/delete/{id}', 'FilmsController@delete');
});

Route::get('/users', 'UserController@index')->name('users.index');
Route::post('/users/search', 'UserController@search')->name('users.search');
Auth::routes();
